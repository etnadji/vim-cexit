# vim-cexit

Clean and quick exit command for (G)Vim.

## Installation

Use your plugin manager of choice.

- [Pathogen](https://github.com/tpope/vim-pathogen)
  - `git clone https://github.com/etnadji/vim-cexit ~/.vim/bundle/vim-cexit`
- [Vundle](https://github.com/gmarik/vundle)
  - Add `Bundle 'https://github.com/etnadji/vim-cexit'` to .vimrc
  - Run `:BundleInstall`
- [NeoBundle](https://github.com/Shougo/neobundle.vim)
  - Add `NeoBundle 'https://github.com/etnadji/vim-cexit'` to .vimrc
  - Run `:NeoBundleInstall`
- [vim-plug](https://github.com/junegunn/vim-plug)
  - Add `Plug 'https://github.com/etnadji/vim-cexit'` to .vimrc
  - Run `:PlugInstall`
