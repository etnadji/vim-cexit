# -*- coding: utf-8 -*-

import os

def remove_pyc(folder):
    for f in curdir.files("*.pyc"):
        os.remove(os.path.realpath(f))

def remove_temp_latex(folder):
    extensions = [
        "aux","run.xml","bbl","bcf","blg","nav","bib","snm","log",
        "toc","out","fls","fdb_latexmk"
    ]

    removable = []
    for extension in extensions:
        removable += folder.files("*.{0}".format(extension))

    print removable

    for f in removable:
        os.remove(os.path.realpath(f))

def normal(vim,keys,silent=True,end_in_normal_mode=False):
    """
    Do normal mode moves/commands.

    keys (str): sequence to execute in normal mode
    silent (bool): execute with silent! … or not
    end_in_normal_mode: Make Vim return into normal mode at the end of the sequence.
    """

    if end_in_normal_mode:
        if not keys.endswith("\<esc>"):
            keys = keys + "\<esc>"

    if silent:
        normal_maps = ':silent! execute "normal! {0}"'.format(keys)
    else:
        normal_maps = ':execute "normal! {0}"'.format(keys)

    vim.command(normal_maps)

def get_current_line(vim):
    """Shorcut function to get vim current buffer's current line"""

    try:
        return vim.current.buffer[vim.current.window.cursor[0]-1]
    except IndexError:
        return False

def get_next_line(vim):
    """
    Shorcut function to get current  vim buffer's line next to the
    current one.
    """

    try:
        return vim.current.buffer[vim.current.window.cursor[0]]
    except IndexError:
        return False

def get_user_input(vim,prompt,erase=True):
    """Use g:VimEPUB_Prompt as a variable to get user input"""

    vim.command('let g:VimEPUB_Prompt = input("{0} ")'.format(prompt))
    print " "

    uinput = to_unicode_or_bust(vim.eval("g:VimEPUB_Prompt"))

    if erase:
        vim.command('let g:VimEPUB_Prompt = "None"')

    return uinput

def echom(vim,message):
    """Shorcut function for :echom"""
    vim.command("""echom '{0}'""".format(message))

def to_unicode_or_bust(obj, encoding='utf-8'):
    """By Kumar McMillan"""

    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj = unicode(obj, encoding)
    return obj

def desunicode(s): return s.encode("utf8")
