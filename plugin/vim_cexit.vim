python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))

let g:Cexit_MakeView = "True"
let g:Cexit_TryGitCommit = "True"

function! Cexit()
python << endOfPython
import os

import vim

from path import path

from vim_cexit import *

triggers = {"latex":False,"python":False}

curdir = os.path.realpath(os.curdir)
curdir = path(curdir)

for buffer in vim.buffers:
    buff = buffer.name.decode("utf8")

    if buff.endswith(".tex"):
        triggers["latex"] = True
    if buff.endswith(".py"):
        triggers["python"] = True

do = False
for v in triggers.values():
    if v:
        do = True; break

if do:

    # Efface les fichiers temporaires
    if triggers["latex"]:
        remove_temp_latex(curdir)

    # Efface les pyc
    if triggers["python"]:
        remove_pyc(curdir)

if vim.eval("g:Cexit_MakeView").lower() == "true":
    vim.command(":mkview")

if vim.eval("g:Cexit_TryGitCommit").lower() == "true":
    code = os.system('git commit -v -a')

vim.command(":wqa")
endOfPython
endfunction

command! Cexit call Cexit()
